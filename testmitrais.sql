-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 19 Mar 2020 pada 09.08
-- Versi server: 10.3.16-MariaDB
-- Versi PHP: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `testmitrais`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `register`
--

CREATE TABLE `register` (
  `id` int(11) NOT NULL,
  `gender` varchar(500) DEFAULT NULL,
  `email` varchar(500) DEFAULT NULL,
  `date_of_birth` datetime DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `mobile_number` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `register`
--

INSERT INTO `register` (`id`, `gender`, `email`, `date_of_birth`, `first_name`, `last_name`, `mobile_number`) VALUES
(29, 'Male', 'robinpotter44@gmail.com', NULL, 'fiverr', 'indonesia', '081387806019'),
(30, 'Male', 'fiverrindonesia@gmail.com', NULL, 'arafik', 'setiawan', '085729835634'),
(31, 'Male', 'fiverrindonesia@gmail.com', NULL, 'fiverr', 'indonesia', '085729835634'),
(32, 'Male', 'fiverrindonesia@gmail.com', NULL, 'fiverr', 'indonesia', '085729835634'),
(33, 'Male', 'robinpotter44@gmail.com', NULL, 'fiverr', 'indonesia', '081387806019'),
(34, 'Male', 'robinpotter44@gmail.com', NULL, 'fiverr', 'indonesia', '081387806019'),
(35, 'Male', 'fiverrindonesia@gmail.com', NULL, 'fiverr', 'indonesia', '085729835634'),
(36, 'Male', 'robinpotter44@gmail.com', NULL, 'fiverr', 'indonesia', '081387806019'),
(37, 'Male', 'fiverrindonesia@gmail.com', NULL, 'fiverr', 'indonesia', '085729835634');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `register`
--
ALTER TABLE `register`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `register`
--
ALTER TABLE `register`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
